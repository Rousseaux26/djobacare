<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        // $manager->persist($product);

        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            '$argon2id$v=19$m=65536,t=4,p=1$U1pYbVVpU0RjRFNWNEdkbg$U2j1vD99sc8OGgjTj+sDxrQveEIVKQdzCiFFSB5Jd4o'
        ));

        $manager->flush();
    }
}
/* mdp testtest : $argon2id$v=19$m=65536,t=4,p=1$U1pYbVVpU0RjRFNWNEdkbg$U2j1vD99sc8OGgjTj+sDxrQveEIVKQdzCiFFSB5Jd4o     */