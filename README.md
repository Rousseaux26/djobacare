# vuejs-symfony-spa
SPA with Vuejs, vue-router, vueX, symfony api platform with auth token started template

# install js packages (Yarn)
- yarn install

# install bundle symfony
- composer update

# launch js server
- yarn watch

# launch symfony server 
- symfony server:start
